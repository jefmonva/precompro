<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Message Sockets</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        
        
    </head>
    <body>
        <h1>Message</h1>

        <div class="container">
            <div id="messages">

            </div>

        </div>



        <script src="https://cdn.socket.io/4.6.0/socket.io.min.js" integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous"></script>
        <!-- <script>
            const ipServer = 'http://127.0.0.1';
            const PortSocket = '3000';
            const socket = io(ipServer + ':' + PortSocket);

            const messages = document.getElementById('messages');

            let data = "Esto es un ejemplo";

            socket.on('pedidoCreated', (data) => {
                messages.innerHTML += '<p>' + data + '</p>';
                emmit('pedidoCreated', data);
            })
        </script> -->
    </body>
</html>
