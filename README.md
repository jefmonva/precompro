
## Configuración

Se debe crear la base de datos "doublevpartners" y actializar el .env con los siguientes datos:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=doublevpartners
DB_USERNAME=root
DB_PASSWORD=
```

Depues de haber agregado estas variables de entorno, ejecutar el comando ```php artisan config:cache``` para actualizar la caché de configuración de la aplicación. Despues ejecutar el comando ```php artisan migrate``` para crear las tablas de la aplicación.

Puede tener datos de ejemplo iniciales corriendo los seeders con el comando ```php artisan db:seed --class=CuentaSeeder``` y ```php artisan db:seed --class=PedidoSeeder```


