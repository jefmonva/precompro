<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\PedidoService;

class PedidoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(PedidoService::class, function ($app) {
            return new PedidoService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
