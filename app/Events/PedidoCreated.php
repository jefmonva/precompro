<?php

namespace App\Events;

use App\Models\Cuenta;
use App\Models\Pedido;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PedidoCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pedido;
    public $cuenta;

    /**
     * Create a new event instance.
     */
    public function __construct(Pedido $pedido, Cuenta $cuenta)
    {
        $this->pedido = $pedido;
        $this->cuenta = $cuenta;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): Channel
    {
        return new Channel('pedidoCreated');
        
    }
}
