<?php

namespace App\Services;

use App\Models\Pedido;
use App\Models\Cuenta;
use App\Events\PedidoCreated;
use Illuminate\Support\Facades\DB;

class PedidoService
{
    public function createPedidoWithAccountInfo(array $pedidoData)
    {
        return DB::transaction(function () use ($pedidoData) {
            $cuenta = Cuenta::find($pedidoData['idCuenta']);
            if (!$cuenta) {
                throw new \Exception('Cuenta no encontrada');
            }

            $pedido = Pedido::create($pedidoData);

            event(new PedidoCreated($pedido, $cuenta));

            return $pedido;
        });
    }
}