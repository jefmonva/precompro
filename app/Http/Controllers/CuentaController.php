<?php

namespace App\Http\Controllers;

use App\Models\Cuenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $cuentas = Cuenta::paginate(10);

        return response()->json([
            'code' => 200,
            'status' => 'success',
            'total_results' => $cuentas->total(),
            'cuentas' => $cuentas->items(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'nombre' => 'required|string|max:255',
                'email' => 'required|email|unique:cuentas,email',
                'telefono' => 'nullable|string|max:20',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Error en los datos proporcionados.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $cuenta = Cuenta::create([
                'nombre' => $request->input('nombre'),
                'email' => $request->input('email'),
                'telefono' => $request->input('telefono'),
            ]);

            return response()->json([
                'code' => 201,
                'status' => 'success',
                'message' => 'Cuenta creada exitosamente.',
                'cuenta' => $cuenta,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {

            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:cuentas,idCuenta',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'ID inválido.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $cuenta = Cuenta::find($id);

            if (!$cuenta) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Cuenta no encontrada.',
                ], 404);
            }

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'cuenta' => $cuenta,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {

            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:cuentas,idCuenta',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'ID inválido.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $cuenta = Cuenta::find($id);

            if (!$cuenta) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Cuenta no encontrada.',
                ], 404);
            }

            $validator = Validator::make($request->all(), [
                'nombre' => 'string|max:255',
                'email' => 'email|unique:cuentas,email,' . $id,
                'telefono' => 'nullable|string|max:20',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Error en los datos proporcionados.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $cuenta->fill($request->all());
            $cuenta->save();

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' => 'Cuenta actualizada exitosamente.',
                'cuenta' => $cuenta,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $validator = Validator::make(['id' => $id], [
                'id' => 'required|integer|exists:cuentas,idCuenta',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'ID inválido.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $cuenta = Cuenta::find($id);

            if (!$cuenta) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Cuenta no encontrada.',
                ], 404);
            }

            $cuenta->delete();

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' => 'Cuenta eliminada exitosamente.',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }
}