<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Services\PedidoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pedidos = Pedido::paginate(10);

        return response()->json([
            'code' => 200,
            'status' => 'success',
            'total_results' => $pedidos->total(),
            'pedidos' => $pedidos->items(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, PedidoService $pedidoService)
    {
        try {

            $validator = Validator::make($request->all(), [
                'idCuenta' => 'required|exists:cuentas,idCuenta',
                'producto' => 'required',
                'cantidad' => 'required|integer|min:1',
                'valor' => 'required|numeric|min:0',
                'total' => 'required|numeric|min:0',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Error en los datos proporcionados.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            // $pedido = Pedido::create($request->all());
            $pedido = $pedidoService->createPedidoWithAccountInfo($request->all());

            return response()->json([
                'code' => 201,
                'status' => 'success',
                'message' => 'Pedido creado exitosamente.',
                'pedido' => $pedido,
            ], 201);

            
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.' . $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {

            $pedido = Pedido::find($id);

            if (!$pedido) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Pedido no encontrado.',
                ], 404);
            }

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'pedido' => $pedido,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'idCuenta' => 'exists:cuentas,idCuenta',
                'idProducto' => 'exists:productos,idProducto',
                'cantidad' => 'integer|min:1',
                'valor' => 'numeric|min:0',
                'total' => 'numeric|min:0',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Error en los datos proporcionados.',
                    'errors' => $validator->errors(),
                ], 400);
            }

            $pedido = Pedido::find($id);

            if (!$pedido) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Pedido no encontrado.',
                ], 404);
            }

            $pedido->fill($request->all());
            $pedido->save();

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' => 'Pedido actualizado exitosamente.',
                'pedido' => $pedido,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {

            $pedido = Pedido::find($id);

            if (!$pedido) {
                return response()->json([
                    'code' => 404,
                    'status' => 'error',
                    'message' => 'Pedido no encontrado.',
                ], 404);
            }

            $pedido->delete();

            return response()->json([
                'code' => 200,
                'status' => 'success',
                'message' => 'Pedido eliminado exitosamente.',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'status' => 'error',
                'message' => 'Ha ocurrido un error en el servidor.',
            ], 500);
        }
    }
}
