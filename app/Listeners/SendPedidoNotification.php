<?php

namespace App\Listeners;

use App\Events\PedidoCreated;
use Workerman\Worker;
use PHPSocketIO\SocketIO;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;


class SendPedidoNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(PedidoCreated $event): void
    {
        $pedido = $event->pedido;
        $cuenta = $event->cuenta;

        $message = "Nuevo pedido de $pedido->idProducto en la cuenta $cuenta->idCuenta";

        // Crear una instancia del servidor Socket.IO
        $io = new SocketIO(3000);

        // Escuchar la conexión de un cliente
        $io->on('connection', function ($socket) use ($io, $message) {
            // Cuando el cliente emite 'pedidoCreated', esto escucha y ejecuta
            $socket->on('pedidoCreated', function ($data) use ($io, $message, $socket) {
                // Emitir el evento 'pedidoCreated' a todos los clientes conectados
                $socket->emit('pedidoCreated', $message);
            });
        });

        // Iniciar el servidor Socket.IO
        $worker = new Worker("127.0.0.1:3000");
        $worker->onWorkerStart = function () use ($io) {
            $io->attach($this);
            Worker::runAll();
        };
    }
}