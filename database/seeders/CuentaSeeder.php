<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['nombre' => 'Juan Pérez', 'email' => 'juan@perez.com', 'telefono' => '3123456789'],
            ['nombre' => 'María González', 'email' => 'maria@gonzalez.com', 'telefono' => '3129876543'],
            ['nombre' => 'Pedro López', 'email' => 'pedro@lopez.com', 'telefono' => '3124567890'],
            ['nombre' => 'Sandra Ramírez', 'email' => 'sandra@ramirez.com', 'telefono' => '3128765432'],
            ['nombre' => 'Luisa García', 'email' => 'luisa@garcia.com', 'telefono' => '3126543210'],
            ['nombre' => 'Carlos Hernández', 'email' => 'carlos@hernandez.com', 'telefono' => '3125432109'],
            ['nombre' => 'Ana Rodríguez', 'email' => 'ana@rodriguez.com', 'telefono' => '3124321098'],
            ['nombre' => 'David Castillo', 'email' => 'david@castillo.com', 'telefono' => '3123210987'],
            ['nombre' => 'Estefanía Martínez', 'email' => 'estefania@martinez.com', 'telefono' => '3122109876'],
        ];

        DB::table('cuentas')->insert($data);
    }
}
