<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('pedidos')->insert([
            'idCuenta' => 1,
            'producto' => 'Producto 1',
            'cantidad' => 2,
            'valor' => 50.00,
            'total' => 100.00,
        ]);

        DB::table('pedidos')->insert([
            'idCuenta' => 2,
            'producto' => 'Producto 2',
            'cantidad' => 3,
            'valor' => 25.00,
            'total' => 75.00,
        ]);
    }
}
